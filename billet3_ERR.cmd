-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Gare g3
!create g3:Gare
!set g3.nom := 'PLACE'

-- Gare g4 
!create g4:Gare
!set g4.nom := 'ART'

-- Gare g5
!create g5:Gare
!set g5.nom:= 'JARRY'

-- Train t1
!create t2:Train
!set t2.immat := 'TRAIN2'
!set t2.gare := g3


-- Billet b2
!create b2:Billet
!set b1.train := 'TRAIN2'
!set b1.origine := g3
!set b1.destination := g4
!set b1.classe = 1
!set b1.trancheAgeMin := 20
!set b1.trancheAgeMax := 29

!insert(t1, g3) into TrainGares
!insert(t1, g5) into TrainGares
