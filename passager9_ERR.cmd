-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Passager p2
!create p2:Passager
!set p1.nom := 'ZORRO'
!set p1.age := 21

-- Gare gc3
!create gc3:Gare
!set gc3.nom := 'MUGI'

-- Gare gc4
!create gc4:Gare
!set gc4.nom := 'DRESSRO'

-- Train t4
!create t4:Train
!set t4.immat := 'TRAIN4'
!set t3.gare := gc3 

-- Billet b4
!create b4:Billet
!set b4.train := 'TRAIN4'
!set b4.origine := gc3
!set b4.destination := gc4
!set b4.classe := 3
!set b4.trancheAgeMin := 18
!set b4.trancheAgeMin: = 34

-- WagonPassager w2
!create w2:WagonPassager
!set w2.classe := 3
!set w2.maxPassagers := 10

!insert(t4, gc3) into TrainGares
!insert(t4, gc4) into TrainGares
!insert(t4, w2) into TrainWagons
!insert(p2, b4) into PassagerBillets

-- appeler la methode entrer(wagon : WagonPassager, billet : Billet) sur p2
!openter p2 entrer(w2, b4)

!opexit
