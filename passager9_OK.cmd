-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Passager p1
!create p1:Passager
!set p1.nom := 'SAMIA'
!set p1.age := 23

-- Gare gc1
!create gc1:Gare
!set gc1.nom := 'RAFTEL'

-- Gare gc2
!create gc2:Gare
!set gc2.nom := 'MARINEFORD'

-- Train t3
!create t3:Train
!set t3.immat:= 'TRAIN3'
!set t3.gare:= gc1 

-- Billet b3
!create b3:Billet
!set b3.train:= 'TRAIN3'
!set b3.origine:= gc1
!set b3.destination:= gc2
!set b3.classe:= 2
!set b3.trancheAgeMin:= 20
!set b3.trancheAgeMin:= 34

-- WagonPassager w1
!create w1:WagonPassager
!set w1.classe:= 2
!set w1.maxPassagers:= 10

!insert(t3, gc1) into TrainGares
!insert(t3, gc2) into TrainGares
!insert(t3, w1) into TrainWagons
!insert(p1, b3) into PassagerBillets

-- appeler la methode entrer(wagon : WagonPassager, billet : Billet) sur p1
!openter p1 entrer(w1, b3)
!insert(w1, p1) into WagonPassagers
!opexit
