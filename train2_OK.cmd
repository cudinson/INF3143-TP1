-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Gare g5
!create g5:Gare
!set g5.train:= 'BRUNO'

-- Gare g6
!create g6:Gare
!set g6.train:= 'PRIVAT'

-- Train
!create t5:Train
!set t4.gare:= g5

-- Container c1
!create c1:Container
!set dangereux:= true

-- WagonContainer wc1
!create wc1:WagonContainer

!insert(t5, g5)into TrainGares
!insert(t5, g6)into TrainGares
!insert(wc1, c1)into WagonContainers
!insert(t5, wc1)into TrainWagons
