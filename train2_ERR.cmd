-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Gare g7
!create g7:Gare
!set g7.train:= 'BEERUS'

-- Gare g8
!create g8:Gare
!set g8.train:= 'JIREN'

-- Train t6
!create t6:Train
!set t6.gare:= g7

-- Container c2
!create c2:Container
!set dangereux:= true

-- WagonContainer wc2
!create wc2:WagonContainer

-- WagonCiterne wi2
!create wci1:WagonCiterne
!set wi2.type:= 'eau'
!set wi2.capacite:= 20
!set wi2.quantite:= 4
!set wi2.dangereux:= false

-- WagonPassager wp2
!create wp2:WagonPassager
!set wp2.classe:=2
!set wp2.maxPassagers:=7

!insert(t6, g7)into TrainGares
!insert(t6, g8)into TrainGares
!insert(wc2, c2)into WagonContainers
!insert(t6, wc2)into TrainWagons
!insert(t6, wi2)into TrainWagons
!insert(t6, wp2)into TrainWagons
