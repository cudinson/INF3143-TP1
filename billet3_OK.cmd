-- OCL TP1
-- FAUJ08079202 - Jefferson Faustin

-- Gare g1
!create g1:Gare
!set g1.nom := 'BERRI'

-- Gare g2
!create g2:Gare
!set g2.nom := 'UQAM'

-- Train
!create t1:Train
!set t1.immat := 'TRAIN1'
!set t1.gare := g1

-- Billet b1
!create b1:Billet
!set b1.train := 'TRAIN1'
!set b1.origine := g1
!set b1.destination := g2
!set b1.classe := 2
!set b1.trancheAgeMin := 18
!set b1.trancheAgeMax : = 30

!insert(t1, g1) into TrainGares
!insert(t1, g2) into TrainGares

